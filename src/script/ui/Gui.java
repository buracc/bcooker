package script.ui;

import net.miginfocom.swing.MigLayout;
import org.rspeer.runetek.api.Game;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import script.Cooker;
import script.data.CookingTask;
import script.data.ScriptSetting;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public final class Gui extends Task {

    private boolean validate = true;
    private final ScriptSetting settings = ScriptSetting.getInstance();

    private final JFrame frame;
    private final JPanel root;

    private final JTextField foodName;
    private final JSpinner foodAmount;
    private final JList<CookingTask> taskList;
    private final DefaultListModel<CookingTask> taskListModel;
    private final JButton addFoodTaskBtn;

    private final JButton startBtn;

    public Gui(Cooker ctx) {
        ScriptMeta meta = ctx.getMeta();

        frame = new JFrame();
        root = new JPanel(new MigLayout());
        frame.setTitle(meta.name() + " v" + meta.version() + " - by " + meta.developer());
        frame.add(root);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getInsets().set(10, 10, 10, 10);
        frame.setResizable(false);

        foodName = new JTextField();
        foodAmount = new JSpinner(new SpinnerNumberModel(0, 0, 10000, 1));
        startBtn = new JButton("Start");

        taskListModel = new DefaultListModel<>();
        taskList = new JList<>(taskListModel);
        addFoodTaskBtn = new JButton("Add task");
        addFoodTaskBtn.addActionListener(e -> addTaskHandler());
        taskList.setFixedCellWidth(150);
        taskList.setFixedCellHeight(16);

        startBtn.addActionListener(e -> startBtnHandler());

        JScrollPane taskListScroll = new JScrollPane();
        taskListScroll.setBorder(new TitledBorder("Tasks"));
        taskListScroll.setViewportView(taskList);
        root.add(taskListScroll, "spany 4, growx");
        root.add(new JLabel("Food name: "), "grow");
        root.add(foodName, "grow, wrap");
        root.add(new JLabel("Food amount: "), "grow");
        root.add(foodAmount, "grow, wrap");
        root.add(addFoodTaskBtn, "grow, spanx 2, wrap");
        root.add(startBtn, "grow");

        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(Game.getCanvas());
    }

    private void addTaskHandler() {
        CookingTask task = new CookingTask(foodName.getText(), (int) foodAmount.getValue());
        taskListModel.addElement(task);
        settings.getTasks().add(task);
    }

    private void startBtnHandler() {
        root.setVisible(false);
        validate = false;
        frame.dispose();
    }

    @Override
    public boolean validate() {
        return validate;
    }

    @Override
    public int execute() {
        return 1000;
    }

    @Override
    public String toString() {
        return "Gui";
    }
}
