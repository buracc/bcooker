package script;

import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.listeners.SkillListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.runetek.event.types.SkillEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import script.tasks.Banking;
import script.tasks.Cooking;
import script.ui.Gui;

import java.awt.*;

@ScriptMeta(developer = "burak", desc = "Cooks food", name = "bCooker")
public final class Cooker extends TaskScript implements RenderListener, SkillListener {

    private int startExperience;
    private int startLevel;
    private int expGained;
    private StopWatch timer = StopWatch.start();

    private Task[] tasks = {
            new Gui(this),
            new Banking(),
            new Cooking(),
    };

    @Override
    public void onStart() {
        if (Game.isLoggedIn() && startExperience == 0) {
            startExperience = Skills.getExperience(Skill.COOKING);
        }

        if (Game.isLoggedIn() && startLevel == 0) {
            startLevel = Skills.getLevel(Skill.COOKING);
        }

        submit(tasks);
    }

    @Override
    public void notify(RenderEvent e) {
        Graphics g = e.getSource();

        int x = 372;
        int y = 19;

        g.setColor(new Color(0, 0, 0, 0.5f));
        g.fillRect(x - 5, y - 15, 150, 100);

        g.setColor(Color.decode("#B63EA3"));
        g.drawString(getMeta().name() + " v" + getMeta().version(), x, y);
        g.setColor(Color.GREEN);

        g.drawString("Runtime: " + timer.toElapsedString(), x, y += 11);
        g.drawString("Exp gained: " + expGained + " (" + Math.round(timer.getHourlyRate(expGained)) + ")", x, y += 11);
        g.drawString("Current level: " + Skills.getLevel(Skill.COOKING) + " (" + startLevel + ")", x, y += 11);
        g.drawString("Task: " + (getCurrent() != null ? getCurrent().toString() : "Idle"), x, y += 11);

    }

    @Override
    public void notify(SkillEvent e) {
        if (e.getType() == SkillEvent.TYPE_EXPERIENCE && e.getSource().equals(Skill.COOKING)) {
            expGained += e.getCurrent() - e.getPrevious();
        }
    }
}

