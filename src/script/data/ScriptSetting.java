package script.data;

import java.util.ArrayList;
import java.util.List;

public final class ScriptSetting {

    private static ScriptSetting instance;
    private List<CookingTask> tasks = new ArrayList<>();

    public static ScriptSetting getInstance() {
        if (instance == null) {
            instance = new ScriptSetting();
        }

        return instance;
    }

    public List<CookingTask> getTasks() {
        return tasks;
    }
}
