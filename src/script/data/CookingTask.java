package script.data;

public final class CookingTask {

    private String food;
    private int amount;

    public CookingTask(String food, int amount) {
        this.food = food;
        this.amount = amount;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return food + ", " + amount + "x";
    }


}
