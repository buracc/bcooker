package script.tasks;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Definitions;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Production;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ItemTableListener;
import org.rspeer.runetek.event.types.ItemTableEvent;
import org.rspeer.runetek.providers.RSItemDefinition;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;
import script.data.CookingTask;
import script.data.ScriptSetting;

public final class Cooking extends Task implements ItemTableListener {

    private final ScriptSetting settings = ScriptSetting.getInstance();
    private long animationTimer = 0;
    private static final int FIRE_ID = 26185;
    private String currentFood;
    private RSItemDefinition foodDef;

    @Override
    public boolean validate() {
        for (CookingTask task : settings.getTasks()) {
            if (task.getAmount() > 0) {
                currentFood = task.getFood();
            }
        }

        return Inventory.getFirst(currentFood) != null && !isAnimating();
    }

    @Override
    public int execute() {
        SceneObject fire = SceneObjects.getNearest(FIRE_ID);

        if (!Production.isOpen()) {
            if (fire != null) {
                Inventory.use(x -> x.getName().equals(currentFood), fire);
            }
            return 600;
        }

        Production.initiate();
        return 600;
    }

    private boolean isAnimating() {
        if (Players.getLocal().isAnimating()) {
            animationTimer = System.currentTimeMillis();
            return true;
        }

        return System.currentTimeMillis() <= (animationTimer + Random.high(2000, 3000));
    }

    @Override
    public String toString() {
        return "Cooking";
    }

    @Override
    public void notify(ItemTableEvent e) {
        foodDef = Definitions.getItem(currentFood, x -> !x.isNoted());

        if ((foodDef != null && !foodDef.equals(e.getOldDefinition())) || Bank.isOpen()) {
            return;
        }

        Item rawFood = Inventory.getFirst(currentFood);
        if (rawFood != null) {
            for (CookingTask task : settings.getTasks()) {
                if (task.getFood().equals(currentFood)) {
                    Log.info(currentFood + " : " + task.getAmount());
                    task.setAmount(task.getAmount() - 1);
                }
            }
        }
    }
}
