package script.tasks;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;
import script.data.CookingTask;
import script.data.ScriptSetting;

public final class Banking extends Task {

    private final ScriptSetting settings = ScriptSetting.getInstance();
    private String currentFood;
    private int foodAmount;

    @Override
    public boolean validate() {
        for (CookingTask task : settings.getTasks()) {
            if (task.getAmount() > 0) {
                currentFood = task.getFood();
                foodAmount = task.getAmount();
            }
        }

        return Inventory.getFirst(currentFood) == null;
    }

    @Override
    public int execute() {
        Item food = Inventory.getFirst(currentFood);

        if (Bank.isOpen()) {
            if (food == null && !Inventory.isEmpty()) {
                Bank.depositInventory();
                return 600;
            }

            Bank.withdraw(currentFood, foodAmount);
            return 600;
        }

        Bank.open();
        return 600;
    }

    @Override
    public String toString() {
        return "Banking";
    }
}
